steps = [
    [
        ## Create the table
        """
        CREATE TABLE vacations (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(100) NOT NULL,
            from_date DATE NOT NULL,
            to_date DATE NOT NULL,
            thoughts TEXT
        );
        """,

        ## Drop the table, destroys table
        """
        DROP TABLE vacations;
        """
    ]
]
