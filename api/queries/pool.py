import os
from psycopg_pool import ConnectionPool

#pool object will be used to get connections to the DB
#import pool to queries.vacations
pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])
