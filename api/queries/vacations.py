from pydantic import BaseModel
from typing import Optional
from datetime import date
from queries.pool import pool

# queries is where we write SQL

#BaseModel has nothing to do with database.
# It is determine shape of data for endpoint

class Error(BaseModel):
    message: str

#vacationin is what the user enters into the fields
#this is a pydantic model, all have a dict property
class VacationIn(BaseModel):
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]

#what gets spit out
class VacationOut(BaseModel):
    id: int
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]


#this is to connect to the DB
class VacationRepo:
    def create(self, vacation: VacationIn) -> VacationOut:
        #connect to DB by pool of connections (pool.py)
        with pool.connection() as conn:
            #get cursor (something to run SQL with)
            with conn.cursor() as db:
                #run our insert statement
                result = db.execute(
                    """
                    INSERT INTO vacations
                        (name, from_date, to_date, thoughts)
                    VALUES
                        (%s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        vacation.name,
                        vacation.from_date,
                        vacation.to_date,
                        vacation.thoughts
                    ]
                )
                #get the id from tuple
                id = result.fetchone()[0]
                old_data = vacation.dict()
                return VacationOut(id=id, **old_data)
