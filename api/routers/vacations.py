from fastapi import APIRouter, Depends, Response
from typing import Union
from queries.vacations import (
    VacationIn,
    VacationRepo,
    VacationOut,
    Error,
)


# where we write our fastAPI endpoints
router = APIRouter()


@router.post("/vacations", response_model=Union[VacationOut, Error])
# tell fastapi we need json to do stuff > create pydantic model in queries file
# def create_vacation is an endpoint
def create_vacation(
    vacation: VacationIn,
    response: Response,
    repo: VacationRepo = Depends()
):
    #ask fastapi to return a response when there is an error and set status code to 400
    response.status_code = 400
    #.create function comes from VacationRepo def create
    return repo.create(vacation)
